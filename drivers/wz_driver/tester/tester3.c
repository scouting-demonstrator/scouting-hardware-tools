#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdint.h>
#include <time.h>
//#include <wz-xdma-ioctl.h>
#include <fcntl.h>

#define TOT_BUF_LEN ((int64_t) WZ_DMA_BUFLEN * (int64_t) WZ_DMA_NOFBUFS)

#include "wz_dma_utils.c"


struct timespec ts;
double tstart, tcur;

int first = 1;

int64_t tot_len = 0;
int64_t old_tot_len = 0;

int main(int argc, char * argv[])
{
	(void)(argc);
	(void)(argv);
    printf("Getting devices..\n ");

	struct wz_private dma;

	if ( wz_init( &dma ) < 0 ) {
		printf("DMA init failed\n");
		exit(1);
	}

	//Start the data acquisition
	if ( wz_start_dma( &dma ) < 0) {
		perror("I can't start the data source");
		exit(3);
	}

	printf("Starting.. \n");

	/* create file to write data to */
	char *filename = "/tmp/testfile.bin";
	int file_fd = -1;
	if (filename) {
		file_fd = open(filename, O_RDWR | O_CREAT | O_TRUNC | O_SYNC, 0666);
		assert(file_fd >= 0);
	}

	clock_gettime(CLOCK_MONOTONIC,&ts);
	tstart=ts.tv_sec+1.0e-9*ts.tv_nsec;
    
	char *buf;
	int64_t cur_len;

	int count = 1000;
	while(count-- > 0) {        
		// printf("Getting buffer.. \n");
		if ( (cur_len = wz_read_start( &dma, &buf )) < 0) {
			perror("Read failed");
			printf("transmitted: %ld\n", tot_len);
			exit(4);
		}

		if (cur_len != 633632) {
			printf("Read %ld bytes from device.\n", cur_len);
		}
		if(count == 0) {
			printf("Writing to file... \n");
			/* file argument given? */
			if (file_fd >= 0) {
			/* write buffer to file */
			int rc = write(file_fd, /*dma.data_buf + start_offset*/ buf, cur_len);
			assert(rc == cur_len);
			}
		}
		// }

		tot_len += cur_len;

		if (wz_read_complete( &dma ) < 0) {
			perror("I can't complete read");
			exit(4);
		}

	//if(tot_len > 10L*1024L*1024L*1024L) break; //exit, to check if the the program closes cleanly
	}

	clock_gettime(CLOCK_MONOTONIC,&ts);
	tcur=ts.tv_sec+1.0e-9*ts.tv_nsec;
	printf("transmitted: %ld time: %g rate: %g\n",tot_len, tcur-tstart, tot_len/(tcur-tstart));

	wz_stop_dma( &dma );
	wz_close( &dma );

	return 0;
}

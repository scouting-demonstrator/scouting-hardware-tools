#!/bin/bash

ADDRMAP=$1
VENDID=$2
DEVID=$3
DEVIDX=$4
MODE=$5
REGS=$6

export LD_LIBRARY_PATH=/opt/xdaq/lib/
export PYTHONPATH=/opt/xdaq/etc/PyHAL/

if [ $MODE = "read" ]; then
    pcie_reconnect_hal.py -a $ADDRMAP --vendid $VENDID --devid $DEVID --devidx $DEVIDX --mode $MODE
elif [ $MODE = "write" ]; then
    pcie_reconnect_hal.py -a $ADDRMAP --vendid $VENDID --devid $DEVID --devidx $DEVIDX --mode $MODE -r "$REGS"
else
    echo "No mode specified"
fi

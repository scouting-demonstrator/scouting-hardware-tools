#!/usr/bin/env bash

set -e # Fail on error

dev=${1}

if [ -z "$dev" ]; then
    echo "No device ID passed in. Bailing out."
    exit 1
fi

# TODO: Not entirely sure why this would ever not be needed. (It's from the original script from the Xilinx forums.)
if [ ! -e "/sys/bus/pci/devices/$dev" ]; then
    dev="0000:$dev"
fi
if [ ! -e "/sys/bus/pci/devices/$dev" ]; then
    echo "Device ${dev} doesn't appear to be an actual device. Bailing out."
    exit 1
fi
echo "Disabling fatal error reporting on device ID ${dev}."
port=$(basename $(dirname $(readlink "/sys/bus/pci/devices/$dev")))
if [ ! -e "/sys/bus/pci/devices/$port" ]; then
    echo "Error: device $port not found"
    exit 1
fi

echo "Using port $port..."

cmd=$(setpci -s "$port" COMMAND)

# clear SERR bit in command register
setpci -s "$port" COMMAND=$(printf "%04x" $((0x$cmd & ~0x0100)))

ctrl=$(setpci -s "$port" CAP_EXP+8.w)

# clear fatal error reporting enable bit in device control register
setpci -s "$port" CAP_EXP+8.w=$(printf "%04x" $((0x$ctrl & ~0x0004)))


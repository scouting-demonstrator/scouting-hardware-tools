#!/usr/bin/python3.8
from __future__ import print_function

import os
import sys
import time
import optparse
import hal

# Command-line parser
usage = "usage: %prog [options]"
parser = optparse.OptionParser(usage)
parser.add_option("-a", "--addrmap", action="store",      dest="addrmap", type="string", default="vcu128_address_map.dat", help="Address map file"  )
parser.add_option("",   "--vendid",  action="store",      dest="vendid",  type="int",    default=0x10dc,                   help="Vendor ID"         )
parser.add_option("",   "--devid",   action="store",      dest="devid",   type="int",    default=0x01b5,                   help="Device ID"         )
parser.add_option("",   "--devidx",  action="store",	  dest="devidx",  type="int",    default=0,                        help="Device Index"      )
parser.add_option("-m", "--mode",    action="store",      dest="mode",    type="string",                                   help="Mode [read|write]" )
parser.add_option("-r", "--regs",    action="store",      dest="regs",    type="string", default="0",                      help="PCIe registers"    )
(options, args) = parser.parse_args()

# Settings and configuration
ADDRMAP = options.addrmap   # Address Map file
VENDID  = options.vendid    # Vendor ID
DEVID   = options.devid     # Device ID
DEVIDX  = options.devidx    # Device Index
MODE    = options.mode      # Script mode [read|write]
REGS    = options.regs      # Configuration registers (read after running in read mode)

# load address map
vcu128 = hal.PCIDevice(ADDRMAP, VENDID, DEVID, DEVIDX)

# read 16 config words
if MODE=="read":
    # enable PCIe core to be accessed
    pcie_enabled = vcu128.read("PCIe_Command", 0x0)
    if (pcie_enabled & 0x1)==0:
        vcu128.write("PCIe_Command", 0x1, False, 0x0)

    for i in range(0,16):
        conf_val = vcu128.read("Device_Vendor_ID", i*4)
        print(hex(conf_val), end=" ")

# write 16 config words
elif MODE=="write":
    regs_list = REGS.split(" ")[:-1]
    for i,reg in enumerate(regs_list):
        vcu128.write("Device_Vendor_ID", int(reg, 16), False, i*4)

else:
    print("ERROR: script mode not recognized (", MODE, ")")

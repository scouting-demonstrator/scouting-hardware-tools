#!/usr/bin/python3.8
import time
import logging
import optparse

import hal
from I2CInterface import I2CInterface



usage = "usage: %prog [options]"
parser = optparse.OptionParser(usage)
parser.add_option("-a", "--addrmap",   action="store",      dest="addrmap",   type="string", default="address_map_vcu128.dat", help="Address map file"  )
parser.add_option("",   "--vendid",    action="store",      dest="vendid",    type="int",    default=0x10dc,                   help="Vendor ID"         )
parser.add_option("",   "--devid",     action="store",      dest="devid",     type="int",    default=0x01b5,                   help="Device ID"         )
parser.add_option("",   "--devidx",    action="store",      dest="devidx",    type="int",    default=0,                        help="Device Index"      )
parser.add_option("-q", "--qsfpid",    action="store",      dest="qsfpid",    type="int",    default=1,                        help="QSFP number"       )
parser.add_option("-f", "--freq",      action="store",      dest="freq",      type="string", default="156.25",                 help="New frequency"     )
parser.add_option("-v", "--verbosity", action="store",      dest="verbosity", type="string", default="debug",                  help="Increase verbosity")
(options, args) = parser.parse_args()



### Settings and configuration
ADDRMAP   = options.addrmap   # Address Map file
VENDID    = options.vendid    # Vendor ID
DEVID     = options.devid     # Device ID
DEVIDX    = options.devidx    # Device Index
QSFPID    = options.qsfpid    # Number of QSFP
FREQ      = options.freq      # New frequency
VERBOSITY = options.verbosity # Verbosity

# logger
debug_i2c = False
if VERBOSITY=="debug":
    logLevel = logging.DEBUG
    debug_i2c = True
elif VERBOSITY=="info":
    logLevel = logging.INFO
elif VERBOSITY=="warning":
    logLevel = logging.WARNING
elif VERBOSITY=="error":
    logLevel = logging.ERROR
logging.basicConfig(format='%(levelname)s: %(name)s: %(message)s', level=logLevel)
logger = logging.getLogger("Si570")

# load address map
vcu128 = hal.PCIDevice(ADDRMAP, VENDID, DEVID, DEVIDX)

# enable PCIe core to be accessed
PCIe_enabled = vcu128.read("PCIe_Command", 0x0)
if (PCIe_enabled & 0x1)==0:
    vcu128.write("PCIe_Command", 0x1, False, 0x0)
    logger.debug("PCIe memory access needed enable")



### i2c init
# reset i2c
logger.debug("Resetting I2C...")
vcu128.write64("reset_hw_comp", 0x80000000, False, 0)
time.sleep(0.1)
vcu128.write64("reset_hw_comp", 0, False, 0)
logger.debug("Done")

# i2c devices instantiation
logger.debug("Instantiating I2C interfaces...")
i2c_GPIO   = I2CInterface(vcu128, 0x40, offsetWidth=0x1, dataWidth=0x1, items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'}, debugFlag=debug_i2c)
i2c_switch = I2CInterface(vcu128, 0xE8, offsetWidth=0x0, dataWidth=0x1, items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'}, debugFlag=debug_i2c)
i2c_qsfp   = I2CInterface(vcu128, 0xBA, offsetWidth=0x1, dataWidth=0x1, items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'}, debugFlag=debug_i2c)
logger.debug("Done")

# allowed frequencies
freqs = ["156.25", "250.00", "322.265625"]
if FREQ not in freqs:
    logger.error("Error: selected frequency is not supported. Allowed: 156.25, 250.00, 322.265625 MHz.")
    exit()



### QSFP channel
QSFP_ch = 1 << (2 + QSFPID)



### functions to compute register configurations
# new register values calculator
def compute_new_vals(f0, new_freq, data_reg):
    logger = logging.getLogger("Si570")

    hs_div   = get_hs_div(data_reg)
    n1       = get_n1(data_reg)
    ref_freq = get_rfreq(data_reg)

    # compute the nominal crystal frequency
    fxtal = (f0 * hs_div * n1) / ref_freq
    fxtal = round(fxtal, 6)

    logger.debug("Read HS, N1, RFREQ")
    logger.debug("    HS_DIV: {}".format(hs_div))
    logger.debug("    N1: {}".format(n1))
    logger.debug("    RFREQ: {:.6f} MHz".format(ref_freq))
    logger.debug("    => Nominal crystal frequency: {:.6f} MHz".format(fxtal))

    # check boundary for the new frequency
    fdco, new_hs_div, new_n1 = find_dividers(new_freq)
    if fdco!=9999:
        logger.debug("Found new dividers")
        logger.debug("    new HS_DIV: {}".format(new_hs_div))
        logger.debug("    new N1: {}".format(new_n1))
        logger.debug("    fdco: {} MHz".format(fdco))
    else:
        logger.error("Dividers not found!")
        exit()

    # compute new reference frequency
    new_ref_freq = int((fdco / fxtal) * 2**28)

    data_wr_reg = write_values(new_ref_freq, new_n1, new_hs_div)

    logger.debug("Configuration for the new frequency")
    for k in data_wr_reg.keys():
        logger.debug("    register {:>2} -> {:>6}".format(hex(k), hex(data_wr_reg[k])))
    return data_wr_reg


def write_values(ref_freq, n1, hs_div):
    """
    Create list with the new configurations
    => list with one entry for register, starting 7
    """
    # value for reg7
    # -> three bits for hs_div
    reg_7 = (hs_div - 4) << 5
    # -> 5 bits for n1
    reg_7 = reg_7 | ((n1 - 1) >> 2)

    # values for reg 8
    # -> two bits for N1
    reg_8 = (n1 - 1) & 0x3
    # -> 6 bits for rfreq
    reg_8 = (reg_8) << 6 | (ref_freq >> 32)

    # values for reg 9
    reg_9 = (ref_freq >> 24) & 0xff

    # values for reg 10
    reg_10 = (ref_freq >> 16) & 0xff

    # values for reg 11
    reg_11 = (ref_freq >> 8) & 0xff

    # values for reg 12
    reg_12 = ref_freq & 0xff

    data_wr_reg = {0x07: reg_7, 0x08: reg_8, 0x09: reg_9, 0x0a: reg_10, 0x0b: reg_11, 0x0c: reg_12}

    return data_wr_reg


def find_dividers(f1):
    """
    Find valid dividers given the target frequency
    Start from a fixed HS_DIV and change N1
    """
    # from p. 14-17 of
    # https://www.skyworksinc.com/-/media/skyworks/sl/documents/public/data-sheets/si570-71.pdf
    valid_hs_div = [4, 5, 6, 7, 9, 11]
    valid_n1 = [1] + [i for i in range(2,130,2)]

    get_fdco = lambda f1, hs_div, n1: f1 * hs_div * n1

    curr_fdco = 9999
    min_diff_curr_dfco = 9999
    div_found = False
    for i,hs_i in enumerate(valid_hs_div):
        for j,n1_j in enumerate(valid_n1):

            fdco = get_fdco(f1, hs_i, n1_j)
            if (fdco<4900) or (fdco>5600):
                continue
            else:
                div_found = True

            # take 5.25 GHz as reference in the search
            diff_curr_fdco = abs(fdco - 5250)
            if diff_curr_fdco < min_diff_curr_dfco:
                min_diff_curr_dfco = diff_curr_fdco
                curr_div = hs_i
                curr_n1  = n1_j
                curr_fdco= fdco

    if div_found:
        return curr_fdco, curr_div, curr_n1
    else:
        return 9999, 4, 7


def check_fdco(fdco):
    """
    The output dividers must ensure that the DCO
    oscillation frequency (fdco) is between
    4.85 GHz <= fdco <= 5.67 GHz
    fdco = f1 * HS_DIV * N1
    """
    if (4900 <= fdco) and (fdco <= 5600):  # conservative choice
        return True
    else:
        return False


def get_rfreq(data_rd_reg):
    """
    Reference Frequency
    => first 6 bits of reg 8 (5 downto 0)
        reg9, reg10, reg11, reg12
    """
    reg_8 = data_rd_reg[1]

    # get bits 5 downto 0
    rfreq = reg_8 & 0x3F

    # concatenate with the value of other registers
    for data_reg in data_rd_reg[2:]:
        rfreq = (rfreq << 8) | data_reg

    return 1.0 * rfreq / 2**28

def get_n1(data_rd_reg):
    """
    CLKOUT output divider
    => first 5 bits of register 7 (4 downto 0)
        last 2 bits of register  8 (7 downto 6)
    """
    reg_7 = data_rd_reg[0]
    reg_8 = data_rd_reg[1]

    n1_7 = reg_7 & 0x3F
    n1_8 = reg_8 >> 6

    res = (n1_7 << 2 | n1_8) + 1
    return res

def get_hs_div(data_rd_reg):
    """
    high speed divider HS_DIV:
    => last 3 bits of register 7
        (bit 7 to 5)
    """
    reg_7 = data_rd_reg[0]

    # map from the datasheet
    # 000 = 4
    # 001 = 5
    # ...
    # 100, 111 not used
    HS_DIV = (reg_7 >> 5) + 4

    if HS_DIV in [8, 9]:
        return 9999
    else:
        return HS_DIV



### configure TCA6416A (addr: 0x20) to drive RESET_B high
logger.debug("TCA6416A (addr: 0x20) configuration (0x06) read:")
value = i2c_GPIO.read(0x06)
logger.debug("0x%2x" % value)
logger.debug("Done")

logger.debug("TCA6416A (addr: 0x20) configuration (0x06) set P05 to output:")
i2c_GPIO.write(0x06, 0xDF)
logger.debug("Done")

time.sleep(0.1)
logger.debug("TCA6416A (addr: 0x20) output port 0 (0x02) read")
value = i2c_GPIO.read(0x02)
logger.debug("0x%2x" % value)
logger.debug("Done")

time.sleep(0.1)
logger.debug("TCA6416A (addr: 0x20) output port 0 (0x02) set P05 to 1")
i2c_GPIO.write(0x02, value | 0x20)
logger.debug("Done")


### enable CH3 on switch
time.sleep(0.1)
logger.debug("TCA9548A (addr: 0x74) enable CH")
i2c_switch.write(0x00, QSFP_ch)
logger.debug("Done")


### reset
time.sleep(0.1)
i2c_qsfp.write(0x87, 0x80)
time.sleep(1.0)


### enable CH3 on switch
time.sleep(0.1)
logger.debug("TCA9548A (addr: 0x74) enable CH")
i2c_switch.write(0x00, QSFP_ch)
logger.debug("Done")


### read default
# reg 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C
def_regs = []
for reg in [0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c]:
    time.sleep(0.1)
    logger.debug("TCA9548A (addr: 0x5D) Reg 0x%2x" % reg)
    def_regs.append(i2c_qsfp.read(reg))
    logger.debug("Done")

logger.info(def_regs)
logger.info([hex(r) for r in def_regs])
logger.info(float(FREQ))
new_regs = compute_new_vals(156.25, float(FREQ), def_regs)
logger.info(new_regs)


### enable QSFP channel on switch
time.sleep(0.1)
logger.info("TCA9548A (addr: 0x74) enable CH")
i2c_switch.write(0x00, QSFP_ch)
logger.debug("Done")


### DCO freeze
time.sleep(0.1)
logger.info("TCA9548A (addr: 0x5D) Freeze DCO")
i2c_qsfp.write(0x89, 0x10)
logger.debug("Done")


### reg 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C
for reg in new_regs.keys():
    time.sleep(0.1)
    logger.info("TCA9548A (addr: 0x5D) Reg 0x%2x" % reg)
    i2c_qsfp.write(reg, new_regs[reg])
    logger.debug("Done")


### DCO unfreeze
time.sleep(0.1)
logger.info("TCA9548A (addr: 0x5D) Unfreeze DCO")
i2c_qsfp.write(0x89, 0x00)
logger.debug("Done")

# new freq bit (within 10 ms after unfreezing the DCO)
time.sleep(0.005)
logger.info("TCA9548A (addr: 0x5D) New freq bit")
i2c_qsfp.write(0x87, 0x40)
logger.debug("Done")

# disable CH on switch
time.sleep(0.5)
logger.info("TCA9548A (addr: 0x74) disable CH")
i2c_switch.write(0x00, 0x00)
logger.debug("Done")
#-------------------------------------------------------------------------------

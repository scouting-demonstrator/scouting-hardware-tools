#!/usr/bin/env bash

dev=${1}

if [ -z "$dev" ]; then
    echo "No device ID passed in. Bailing out."
    exit 1
fi

echo "Found device $dev"
# disconnect the FPGA from the PCI device tree if it is present
echo "Disconnecting FPGA from PCIe..."
echo 1 >/sys/bus/pci/devices/0000:"$dev"/remove
result=$?
if [ $result != 0 ]; then
    echo "Unable to disconnect device. Abort."
    exit 2
fi

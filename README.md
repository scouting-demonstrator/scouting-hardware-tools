# Scouting hardware tools

*Important:* The driver requires the Kernel headers for the installed Kernel. If they haven't yet been installed, you will need to do so manually with `yum install "kernel-devel-uname-r == $(uname -r)"` for the moment.

Includes:
* wz-xdma driver
  * Compiled during RPM installation
  * Automatically loaded
* udev rule to change `/dev/wz-xdma0-*` group ownership to `scouter`
* systemd service to disable fatal error reporting at firmware reload
* Utility scripts installed in `/sbin`:
  * `deploy_scouting_firmware.sh`
    * Halts SCONE and scdaq (and the reset server)
    * Copies a bitfile archive from the Gitlab package repository to the production repository at `/opt/scouting/bitfiles`
    * Loads the bitfile using the script contained in the directory given
    * Notes the deployment in a log file in `/var/log` and in the MOTD
    * Resets the board
    * Restarts SCONE and scdaq (and the reset server)
    * **Usage:** `/sbin/deploy_scouting_firmware.sh [CERN username] [bitfile version] [scouting_board_type] [input_system]`<br />
      e.g., `/sbin/deploy_scouting_firmware.sh dinyar vx.y.z kcu1500 ugmt`
  * `disableFatalErrorReporting.sh` (usually called by service)
  * `pcie_reconnect_xilinx.sh` (usually called by deployment script)

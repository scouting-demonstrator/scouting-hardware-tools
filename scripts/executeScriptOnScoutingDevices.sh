#!/usr/bin/env bash

set -e # Fail on error

script=${1}

if [[ -z ${script} ]]; then
    echo "No executable script passed in, bailing out."
    exit 1
fi

# List of device IDs:
#   10ee is the vendorID for Xilinx
#   10dc is the vendorID for CERN
declare -a vendorIDs=("10ee" "10dc")

echo "Checking for Scouting devices in PCIe tree..."
for vendorID in "${vendorIDs[@]}"; do
    if [ -z "$(sudo lspci -nd "${vendorID}": | awk '{ print $1 }')" ]
    then
        echo "No devices with vendor ID '${vendorID}' found"
    else
        lspci -nd "${vendorID}": | awk '{ print $1}' | xargs -n 1 $script # Example output of lspci command: `82:00.0 0700: 10ee:8038`. We take the first entry.
    fi
done

#include <xdma-ioctl.h>
#define TOT_BUF_LEN 	((int64_t) WZ_DMA_BUFLEN * (int64_t) WZ_DMA_NOFBUFS)

int main()
{
  int ret;
  volatile char *data_buf = NULL;
  volatile uint64_t dummy1 = 0;
  struct wz_xdma_data_block_desc bd;
  struct wz_xdma_data_block_confirm bconf;
  int fd_user = open("/dev/wz-xdma0_user", O_RDWR);
  int fd_ctrl = open("/dev/wz-xdma0_control", O_RDWR);
  int fd_data = open("/dev/wz-xdma0_c2h_0", O_RDWR);
 
  // Allocate buffers in the kernel
  ret = ioctl(fd_data, IOCTL_XDMA_WZ_ALLOC_BUFFERS, 0);
  data_buf = mmap(NULL, TOT_BUF_LEN, PROT_READ|PROT_WRITE, MAP_SHARED, fd_data, 0);
 
  // Ensure all pages are mapped
  uint64_t i;
  for(i=0; i<TOT_BUF_LEN/sizeof(uint64_t); i++)
  	dummy1 += data_buf[i];
 
  // Start the data acquisition
  ret = ioctl(fd_data, IOCTL_XDMA_WZ_START, 0);

  /* create file to write data to */
  char *filename = "testfile.bin";
  int file_fd = -1;
  if (filename) {
    file_fd = open(filename, O_RDWR | O_CREAT | O_TRUNC | O_SYNC, 0666);
    assert(file_fd >= 0);
  }

  // Start the source 
  // Do whatever you need to do with your custom logic
 
  // Read data
  while(1) {
    ret = ioctl(fd_data, IOCTL_XDMA_WZ_GETBUF, (long) &bd);

    int64_t start_offset = (int64_t)bd.first_desc * (int64_t)WZ_DMA_BUFLEN;
    int64_t end_offset = (int64_t)bd.last_desc * (int64_t)WZ_DMA_BUFLEN + (int64_t) bd.last_len;
    int64_t len = end_offset - start_offset;
    uint32_t *dptr = (uint32_t*) (data_buf + start_offset);

    // do something with the data 
    printf("Got %ld bytes starting at offset %lu. Covering %d buffers\n", len, start_offset, bd.last_desc-bd.first_desc+1);

    printf("Writing to file... ");
    /* file argument given? */
    if (file_fd >= 0) {
      /* write buffer to file */
      rc = write(file_fd, dptr, len);
      assert(rc == len);
    }

    // After we're done with the data we return the buffer to the pool by using the CONFIRM ioctl
    bconf.first_desc = bd.first_desc;
    bconf.last_desc = bd.last_desc;
    ret = ioctl(fd_data, IOCTL_XDMA_WZ_CONFIRM, (long) &bconf);

    // if(/* on some condition */) 
    break;
  }
 
  // Stop and clean up
  ioctl(fd_data, IOCTL_XDMA_WZ_STOP, 0);
  munmap((void *)data_buf, TOT_BUF_LEN);
  ioctl(fd_data, IOCTL_XDMA_WZ_FREE_BUFFERS, 0);
 
  close(fd_data);
  close(fd_ctrl);
  close(fd_user);
}

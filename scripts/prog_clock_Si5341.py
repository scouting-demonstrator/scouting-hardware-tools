#!/usr/bin/python3.8
import time
import logging
import optparse

# user packages
import hal
from I2CInterface import I2CInterface



usage = "usage: %prog [options]"
parser = optparse.OptionParser(usage)
parser.add_option("-a", "--addrmap",   action="store",      dest="addrmap",   type="string", default="address_map_vcu128.dat", help="Address map file"  )
parser.add_option("",   "--vendid",    action="store",      dest="vendid",    type="int",    default=0x10dc,                   help="Vendor ID"         )
parser.add_option("",   "--devid",     action="store",      dest="devid",     type="int",    default=0x01b5,                   help="Device ID"         )
parser.add_option("",   "--devidx",    action="store",      dest="devidx",    type="int",    default=0,                        help="Device Index"      )
parser.add_option("-f", "--freq",      action="store",      dest="freq",      type="string", default="156.25",                 help="New frequency"     )
parser.add_option("-v", "--verbosity", action="store",      dest="verbosity", type="string", default="debug",                  help="Increase verbosity")
(options, args) = parser.parse_args()



### Settings and configuration
ADDRMAP   = options.addrmap   # Address Map file
VENDID    = options.vendid    # Vendor ID
DEVID     = options.devid     # Device ID
DEVIDX    = options.devidx    # Device Index
FREQ      = options.freq      # New frequency
VERBOSITY = options.verbosity # Verbosity

# logger
debug_i2c = False
if VERBOSITY=="debug":
    logLevel = logging.DEBUG
    debug_i2c = True
elif VERBOSITY=="info":
    logLevel = logging.INFO
elif VERBOSITY=="warning":
    logLevel = logging.WARNING
elif VERBOSITY=="error":
    logLevel = logging.ERROR
logging.basicConfig(format='%(levelname)s: %(name)s: %(message)s', level=logLevel)
logger = logging.getLogger("Si5341")

# load address map
vcu128 = hal.PCIDevice(ADDRMAP, VENDID, DEVID, DEVIDX)

# enable PCIe core to be accessed
PCIe_enabled = vcu128.read("PCIe_Command", 0x0)
if (PCIe_enabled & 0x1)==0:
    vcu128.write("PCIe_Command", 0x1, False, 0x0)
    logger.debug("PCIe memory access needed enable")



### i2c init
# reset i2c
logger.debug("Resetting I2C...")
vcu128.write64("reset_hw_comp", 0x80000000, False, 0)
time.sleep(0.1)
vcu128.write64("reset_hw_comp", 0, False, 0)
logger.debug("Done")

# i2c devices instantiation
logger.debug("Instantiating I2C interfaces...")
i2c_GPIO   = I2CInterface(vcu128, 0x40, offsetWidth=0x1, dataWidth=0x1, items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'}, debugFlag=debug_i2c)
i2c_switch = I2CInterface(vcu128, 0xEC, offsetWidth=0x0, dataWidth=0x1, items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'}, debugFlag=debug_i2c)
i2c_fmcp   = I2CInterface(vcu128, 0xEE, offsetWidth=0x1, dataWidth=0x1, items={'i2c_a': 'qsfp_i2c_access_a', 'i2c_b': 'qsfp_i2c_access_b', 'i2c_poll': 'qsfp_i2c_access_a_access_done'}, debugFlag=debug_i2c)
logger.debug("Done")

# allowed frequencies
freqs = ["156.25", "250.00"]
if FREQ not in freqs:
    logger.error("Error: selected frequency is not supported. Allowed: 156.25, 250.00 MHz.")
    exit()

regs = {
    "156.25" : "/opt/l1scouting-hardware/scouting-hardware-tools/data/clockbuilder/vcu128_FMCP_Si5341_x6_156p25_regs.txt",
    "250.00" : "/opt/l1scouting-hardware/scouting-hardware-tools/data/clockbuilder/vcu128_FMCP_Si5341_x6_250p00_regs.txt",
}

regs_file = open(regs[FREQ], 'r')



### clock programming
line         = ""
address      = 0
page         = 0
reg_off      = 0
data         = 0
reg_data     = 0
command_i2c  = 0
end_file     = 0
page_mem     = 256


# configure TCA6416A (addr: 0x20) to drive RESET_B high
logger.info("TCA6416A (addr: 0x20) configuration (0x06) read")
value = i2c_GPIO.read(0x06)
logger.debug("0x%2x" % value)
logger.debug("Done")

logger.info("TCA6416A (addr: 0x20) configuration (0x06) set P05 to output")
i2c_GPIO.write(0x06, 0xDF)
logger.debug("Done")

time.sleep(0.1)
logger.info("TCA6416A (addr: 0x20) output port 0 (0x02) read")
value = i2c_GPIO.read(0x02)
logger.debug("0x%2x" % value)
logger.debug("Done")

time.sleep(0.1)
logger.info("TCA6416A (addr: 0x20) output port 0 (0x02) set P05 to 1")
i2c_GPIO.write(0x02, value | 0x20)
logger.debug("Done")


# enable channel on switch
time.sleep(0.5)
logger.info("TCA9548A (addr: 0x74) enable CH")
i2c_switch.write(0x00, 0x01)
logger.debug("Done")
time.sleep(0.5)


# loop until end of file
while end_file != 1:

    # read a line
    line = regs_file.readline()
    # line is empty ? end of file
    if line != '':

        # line is not comment
        if line[0]=="0" or line[0]=="P" or line[0]=="p":

            # execute a pause as request in the file
            if line[0] == "P" or line[0] == "p" :
                time.sleep(0.3)
                logger.info("Pause for calibration")

            else :
                address, data = line.split(",")
                address       = int(address,0)
                reg_data      = int(data,0)

                page          = int(address/256)
                reg_off       = int(address&0xFF)

                # execute command to I2C
                if page != page_mem :

                    # change the page
                    i2c_fmcp.write(0x1,page)

                    page_mem = page

                # write the register value
                i2c_fmcp.write(reg_off,reg_data)

    else :
        logger.debug("Done")
        end_file = 1

# close file
regs_file.close()

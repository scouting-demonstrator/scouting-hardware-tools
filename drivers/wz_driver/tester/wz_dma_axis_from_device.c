/*
 * This file is part of the Xilinx DMA IP Core driver for Linux
 *
 * Copyright (c) 2016-present,  Xilinx, Inc.
 * All rights reserved.
 *
 * This source code is licensed under both the BSD-style license (found in the
 * LICENSE file in the root directory of this source tree) and the GPLv2 (found
 * in the COPYING file in the root directory of this source tree).
 * You may select, at your option, one of the above-listed licenses.
 */

#define _BSD_SOURCE
#define _XOPEN_SOURCE 500
#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <xdma-ioctl.h>


#define TOT_BUF_LEN ((int64_t) WZ_DMA_BUFLEN * (int64_t) WZ_DMA_NOFBUFS)
#include "wz_dma_utils.c"

#define DEVICE_NAME_DEFAULT "/dev/xdma0_c2h_0"
#define SIZE_DEFAULT (32)
#define COUNT_DEFAULT (1)

static struct option const long_opts[] = {
	{"device", required_argument, NULL, 'd'},
	{"size", required_argument, NULL, 's'},
	{"count", required_argument, NULL, 'c'},
	{"file", required_argument, NULL, 'f'},
	{"usleep", required_argument, NULL, 'u'},
	{"help", no_argument, NULL, 'h'},
	{"verbose", no_argument, NULL, 'v'},
	{0, 0, 0, 0}
};

static int test_axis_dma(char *devname, uint64_t size, uint64_t count, uint64_t nb_usleep, char *ofname);
static int no_write = 0;


static void usage(const char *name)
{
	int i = 0;
	fprintf(stdout, "%s\n\n", name);
	fprintf(stdout, "usage: %s [OPTIONS]\n\n", name);
	fprintf(stdout, "Read via SGDMA, optionally save output to a file\n\n");

	fprintf(stdout, "  -%c (--%s) device (defaults to %s)\n",
		long_opts[i].val, long_opts[i].name, DEVICE_NAME_DEFAULT);
	i++;
	fprintf(stdout,
		"  -%c (--%s) size of a single transfer in bytes, default %d.\n",
		long_opts[i].val, long_opts[i].name, SIZE_DEFAULT);
	i++;
	fprintf(stdout, "  -%c (--%s) number of transfers, default is %d.\n",
	       long_opts[i].val, long_opts[i].name, COUNT_DEFAULT);
	i++;
	fprintf(stdout,
		"  -%c (--%s) file to write the data of the transfers\n",
		long_opts[i].val, long_opts[i].name);
	i++;
	fprintf(stdout,
		"  -%c (--%s) amount of us to sleep before reading from DMA\n",
		long_opts[i].val, long_opts[i].name);

	i++;
	fprintf(stdout, "  -%c (--%s) print usage help and exit\n",
		long_opts[i].val, long_opts[i].name);
	i++;
	fprintf(stdout, "  -%c (--%s) verbose output\n",
		long_opts[i].val, long_opts[i].name);
	i++;
}

int main(int argc, char *argv[])
{
	int cmd_opt;
	char *device = DEVICE_NAME_DEFAULT;
	uint64_t size = SIZE_DEFAULT;
	uint64_t count = COUNT_DEFAULT;
	uint64_t nb_usleep = 0;
	char *ofname = NULL;

	while ((cmd_opt = getopt_long(argc, argv, "vhxc:f:u:d:a:s:o:", long_opts,
			    NULL)) != -1) {
		switch (cmd_opt) {
		case 0:
			/* long option */
			break;
		case 'd':
			/* device node name */
			device = strdup(optarg);
			break;
			/* RAM size in bytes */
		case 's':
			size = getopt_integer(optarg);
			break;
		case 'c':
			count = getopt_integer(optarg);
			break;
			/* count */
		case 'f':
			ofname = strdup(optarg);
			break;
			/* usleep */
		case 'u':
			nb_usleep = getopt_integer(optarg);
			break;
			/* usleep */
    		case 'x':
			no_write++;
			break;
		case 'v':
			verbose = 1;
			break;
		case 'h':
		default:
			usage(argv[0]);
			exit(0);
			break;
		}
	}
	if (verbose)
	fprintf(stdout,
		"dev %s, buffer size %ld, count %lu, usleep %lu\n",
		device, size, count, nb_usleep);

	return test_axis_dma(device, size, count, nb_usleep, ofname);
}

/*
 * man 2 write:
 * On Linux, write() (and similar system calls) will transfer at most
 * 	0x7ffff000 (2,147,479,552) bytes, returning the number of bytes
 *	actually transferred.  (This is true on both 32-bit and 64-bit
 *	systems.)
 */

#define RW_MAX_SIZE	0x7ffff000

static ssize_t read_axi_packet_to_buffer(char *fname, int fd, char *buffer, uint64_t size)
{
	ssize_t rc;
	uint64_t to_read = size;

	if (to_read > RW_MAX_SIZE) {
		to_read = RW_MAX_SIZE;
	}

	/* read data from file into memory buffer */
	rc = read(fd, buffer, to_read);
	if (rc <= 0) {
		return rc;
	}	
	return rc;
}


static int test_axis_dma(char *devname, uint64_t size, uint64_t count, uint64_t nb_usleep, char *ofname)
{
	ssize_t rc;
	int64_t bytes_read;
	uint64_t i;
	uint64_t dma_errors = 0;
	char *buffer = NULL;
	int out_fd = -1;
	ssize_t total_bytes_read = 0;
	double avg_time = 0;
	double avg_bw = 0;
	double bw;
	ssize_t min_bytes_read = SSIZE_MAX;
	ssize_t max_bytes_read = 0;

	struct timespec ts_start, ts_end;
	double tstart, tend;
	double total_time = 0;

	struct wz_private dma;

	if ( wz_init( &dma ) < 0 ) {
		printf("DMA init failed\n");
		exit(1);
	}

	/* create file to write data to */
	if (ofname) {
		out_fd = open(ofname, O_RDWR | O_CREAT | O_TRUNC | O_SYNC,
				0666);
		if (out_fd < 0) {
                        fprintf(stderr, "unable to open output file %s, %d.\n",
                                ofname, out_fd);
			perror("open output file");
                        rc = -EINVAL;
                        goto out;
                }
	}

	//Start the data acquisition
	if ( wz_start_dma( &dma ) < 0) {
		perror("I can't start the DMA");
		exit(3);
	}

	rc = clock_gettime(CLOCK_MONOTONIC, &ts_start);
	tstart = ts_start.tv_sec + 1.0e-9 * ts_start.tv_nsec;

	for (i = 0; i < count; i++) {
		if (nb_usleep) {
		    usleep(nb_usleep);
		}
		/* lseek & read data from AXI MM into buffer using SGDMA */

		//bytes_read = read_axi_packet_to_buffer(devname, fpga_fd, buffer, size);

		int tries = 0;
		while (1) {
			bytes_read = wz_read_start( &dma, &buffer );
			if (bytes_read < 0) {
				dma_errors++;
				perror("Read failed");
				if (errno == EIO) {
					fprintf(stderr, "Trying to restart DMA: ");
					wz_stop_dma( &dma );
					if (wz_start_dma( &dma ) < 0) {
						perror("I can't start the DMA");
						exit(4);
					}
					fprintf(stderr, "Success.\n");
					tries++;
					if (tries == 10) {
						fprintf(stderr, "We are not advancing, terminating.\n");
						exit(5);
					}
					continue;
				}
				exit(4);
			}
			break;
		}

		// Should not happen
		assert(bytes_read > 0);

		clock_gettime(CLOCK_MONOTONIC, &ts_end);
		tend = ts_end.tv_sec + 1.0e-9 * ts_end.tv_nsec;

		double tdiff = tend - tstart;
		tstart = tend;

		total_time += tdiff;

		/* a bit less accurate but side-effects are accounted for */
		bw = (double)bytes_read / (tdiff * 1024.0 * 1024.0);
		if (verbose) {
			printf("#%lu: read %ld, time %lf sec., BW = %lf MBytes/sec.\n",
				i, bytes_read, tdiff, bw);
		}

		/* file argument given? */
		if ((out_fd >= 0) & (no_write == 0)) {
			rc = write_from_buffer(ofname, out_fd, buffer, bytes_read);
			if (rc < 0)
				goto out;
		}

		total_bytes_read += bytes_read;
		min_bytes_read = bytes_read < min_bytes_read ? bytes_read : min_bytes_read;
		max_bytes_read = bytes_read > max_bytes_read ? bytes_read : max_bytes_read;
		avg_bw += bw;

		if ( wz_read_complete( &dma ) < 0 ) {
			perror("I can't complete read");
			exit(4);
		}
	}
	avg_time = total_time / (double)count;
	avg_bw /= (double)count;
	printf("** Average BW = %f MBytes/sec., DMA errors = %ld, packet min/max = %ld/%ld, packets = %ld, total bytes read = %ld, total time = %lf nsec, avg time = %lf nsec\n", 
		avg_bw, dma_errors, min_bytes_read, max_bytes_read, count, total_bytes_read, total_time, avg_time);
	rc = 0;

out:

	wz_stop_dma( &dma );
	wz_close( &dma );

	return rc;
}

#!/bin/bash

if [[ ! -e /dev/wz-xdma0_c2h_0 ]] || [[ ! -e /dev/wz-xdma0_h2c_0 ]] || [[ ! -e /dev/wz-xdma0_user ]] ; then
    make install
    modprobe xdma
fi

#!/usr/bin/env bash
# --------------------------------------------------------------------
#
# This script is for management of FPGAs with PCIe connection to a PC.
# any firmware upload will break the PCIe link between FPGA and host,
# and this script re-establishes the link afterwards by removing the
# FPGA device from the PCI tree temporarily and then triggering a scan
# for new devices.
#
# Kristian Harder and Henry Franks, July 2017
#
# version 1.1
#
#
# Added device ids (7021 and 8031) to remove pci devices invidually and support two
# devices. Temp fix. Solution required to deal with multiple devices-- Raghu
#---------------------------------------------------------------------

# Here we will loop over all scouting devices and disconnect them from the PCI tree.
executeScriptOnScoutingDevices.sh "disconnectPciDeviceFromTree.sh"

# Then we rescan the PCI devices
echo "Rescanning PCI devices..."
echo 1 >/sys/bus/pci/rescan
result=$?
if [ $result != 0 ]; then
    echo "Unable to rescan bus. Abort."
    exit 3
fi

echo "Done."

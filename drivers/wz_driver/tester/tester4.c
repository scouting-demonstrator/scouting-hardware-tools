#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdint.h>
#include <time.h>
//#include <wz-xdma-ioctl.h>
#include <fcntl.h>

#define TOT_BUF_LEN ((int64_t) WZ_DMA_BUFLEN * (int64_t) WZ_DMA_NOFBUFS)

#include "wz_dma_utils.c"


struct timespec ts;
double tstart, tcur;

int first = 1;

int64_t tot_len = 0;
int64_t old_tot_len = 0;

int main(int argc, char * argv[])
{
	(void)(argc);
	(void)(argv);
    printf("Getting devices..\n ");

	struct wz_private dma;

	if ( wz_init( &dma ) < 0 ) {
		printf("DMA init failed\n");
		exit(1);
	}

//	//Start the data acquisition
//	if ( wz_start_dma( &dma ) < 0) {
//		perror("I can't start the data source");
//		exit(3);
//	}


	wz_stop_dma( &dma );
	wz_close( &dma );

	return 0;
}

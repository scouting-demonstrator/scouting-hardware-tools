#!/usr/bin/env bash

set -e # Exit on error

BITFILE_REPO=${7:-"/opt/l1scouting-hardware/bitfiles"} # If we were given the seventh parameter we use it. Otherwise default.
[[ "${BITFILE_REPO}" == */ ]] || BITFILE_REPO+="/" # Append slash if necessary.

if [ $# -lt 6 ]; then
    echo "#### Usage:"
    echo "## " "$0" "[CERN username] [bitfile version] [scouting_board_type] [input_system] [board_index] [first_upload] [optional: path to bitfile repository]"
    echo "Default bitfile repository is ${BITFILE_REPO}"
    echo "# Example " "$0" "dinyar vx.y.z kcu1500 ugmt 0 0"
    echo "## Exiting.."
    exit 1
fi

USER=$1
VERSION=$2
typeset -l BOARD_TYPE   # Board name should get the lower case version
typeset -l INPUT_SYSTEM # Board name should get the lower case version
BOARD_TYPE=$3
INPUT_SYSTEM=$4
BOARD_INDEX=$5
ISFIRSTLOAD=$6

if [ $BOARD_TYPE != "sb852" ]; then
    if [ "$(command -v vivado_lab)" ]; then
        echo "Found Xilinx Vivado Lab Edition at $(command -v vivado_lab)"
        VIVADO_EDITION="vivado_lab"
    elif [ "$(command -v vivado)" ]; then
        echo "Found Xilinx Vivado at $(command -v vivado)"
        VIVADO_EDITION="vivado"
    else
        echo "Neither Vivado nor Vivado_lab could not be found in your path but required for bitfile deployment to this board. Exiting."
        exit 1
    fi
fi

BOARD_NAME=${BOARD_TYPE}_${INPUT_SYSTEM}
BASENAME=scout-${BOARD_TYPE}-${INPUT_SYSTEM}-${VERSION}
ARCHIVENAME=${BASENAME}.tar.gz

TOKENFILE=~/.gitlab_read_api_token.secret
if [ -f ${TOKENFILE} ]; then
    # Read in file
    AUTHSTRING=${USER}:$(cat $TOKENFILE)
else
    AUTHSTRING=${USER}
fi

# Stop scdaq if on scoutdaq machines
if [[ `hostname` =~ "scoutdaq" ]]; then
    echo "Stopping scdaq.."
    sudo systemctl stop runSCdaq.service
fi

# Stop control & moni
echo "Stopping SCONE.."
sudo systemctl stop scone.service

# Deploy firmware
echo "Copying bitfile to ${BITFILE_REPO} and marking as currently used"
sudo mkdir -p ${BITFILE_REPO}currently_used/
sudo sh -c "cd ${BITFILE_REPO} && curl --user \"${AUTHSTRING}\" \"https://gitlab.cern.ch/api/v4/projects/scouting-demonstrator%2Fscouting-preprocessor/packages/generic/scout-${BOARD_TYPE}-${INPUT_SYSTEM}/${VERSION}/${ARCHIVENAME}\" | tar xvz && ls -l"
sudo rm -f "${BITFILE_REPO}currently_used/${BOARD_INDEX}" # Remove previous soft link if present
sudo ln -sf "${BITFILE_REPO}${BASENAME}" "${BITFILE_REPO}currently_used/${BOARD_INDEX}"
if [ $BOARD_TYPE != "vcu128" ]; then
    bash "${BITFILE_REPO}currently_used/load_${BOARD_TYPE}"_bitfile.sh "${BOARD_NAME}" "${BITFILE_REPO}" "${VIVADO_EDITION}"
else
    bash "${BITFILE_REPO}currently_used/${BOARD_INDEX}/load_${BOARD_TYPE}"_bitfile.sh "${BOARD_NAME}" "${BOARD_INDEX}" "${BITFILE_REPO}" "${ISFIRSTLOAD}" "${VIVADO_EDITION}"
fi
echo "Noting deployment in" /var/log/"${BOARD_TYPE}"_"${BOARD_INDEX}"_bitfile_deployments.log
sudo sh -c "echo $(date): User ${USER} deployed ${BASENAME} to $(hostname) on board index ${BOARD_INDEX}. >> /var/log/${BOARD_TYPE}_${BOARD_INDEX}_bitfile_deployments.log"
sudo sh -c "sed -i '/Current bitfile (board ${BOARD_INDEX})/d' /etc/motd && echo Current bitfile \(board ${BOARD_INDEX}\): ${BASENAME}. Deployed by ${USER} on $(date). >> /etc/motd"

# Start control & moni again
echo "Restarting SCONE.."
sudo systemctl start scone.service
sleep 1

# Reset the board
echo "Asserting reset"
if [ $BOARD_TYPE != "vcu128" ]; then
    for i in 1 2 3 4 5; do curl -X POST -F "value=1" localhost:8080/"${BOARD_NAME}"/reset_board/write && break || sleep 1; done
    echo "Deasserting reset"
    for i in 1 2 3 4 5; do curl -X POST -F "value=0" localhost:8080/"${BOARD_NAME}"/reset_board/write && break || sleep 1; done
else
    curl -X POST -H "Content-type: application/json" -d '{}' localhost:8080/v2/"${BOARD_NAME}"/"${BOARD_INDEX}"/reset
    echo "Reset done"
fi

# Start scdaq again
if [[ `hostname` =~ "scoutdaq" ]]; then
    read -p "Start scdaq again? [y/n] " -n 1 -r
    echo # (optional) move to a new line
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
        echo "Skipping start of scdaq."
    else
        echo "Restarting scdaq.."
        sudo systemctl start runSCdaq.service
    fi
fi

echo "All done. Have fun!"

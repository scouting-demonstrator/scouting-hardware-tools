%define _prefix  /opt/l1scouting-hardware
%define _sbindir /sbin
%define _modules /etc/modules-load.d
%define _systemd /etc/systemd/system
%define _udev /etc/udev/rules.d
%define _homedir %{_prefix}/scouting-hardware-tools
%define _bitfilerepo %{_prefix}/bitfiles
%define debug_package %{nil}

Name: scouting-hardware-tools
Version: %{_version}
Release: %{_release}
Summary: CMS L1 Scouting tools for accessing the hardware
Group: CMS/L1Scouting
License: GPL
Vendor: CMS/L1Scouting
Packager: %{_packager}
Source: %{name}.tar
ExclusiveOs: linux
Provides: scouting-hardware-tools

Prefix: %{_prefix}

%description
Drivers, udev rules, etc. for CMS L1 Scouting.

%files
%defattr(-,root,root,-)
%attr(-, root, root) %dir %{_prefix}
%attr(-, root, root) %dir %{_homedir}
%attr(-, root, root) %dir %{_bitfilerepo}
%attr(-, root, root) %{_homedir}/drivers
%attr(-, root, root) %{_homedir}/data
%attr(755, root, root) %{_sbindir}/checkAndFixWzdmaPresence.sh
%attr(755, root, root) %{_sbindir}/deploy_scouting_firmware.sh
%attr(755, root, root) %{_sbindir}/disableFatalErrorReporting.sh
%attr(755, root, root) %{_sbindir}/disableFatalErrorReportingForScouting.sh
%attr(755, root, root) %{_sbindir}/pcie_reconnect_xilinx.sh
%attr(755, root, root) %{_sbindir}/disconnectPciDeviceFromTree.sh
%attr(755, root, root) %{_sbindir}/executeScriptOnScoutingDevices.sh
%attr(755, root, root) %{_sbindir}/pcie_reconnect_hal.sh
%attr(755, root, root) %{_sbindir}/pcie_reconnect_hal.py
%attr(755, root, root) %{_sbindir}/prog_clock_Si570.py
%attr(755, root, root) %{_sbindir}/prog_clock_Si5341.py
%attr(755, root, root) %{_sbindir}/I2CInterface.py
%attr(644, root, root) %{_modules}/kcu1500_xdma.conf
%attr(644, root, root) %{_systemd}/checkAndFixWzdmaPresence.service
%attr(644, root, root) %{_systemd}/disableFatalErrorReporting.service
%attr(644, root, root) %{_udev}/99-xdma-permissions.rules

%prep
%setup -c

%build

%install
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_prefix}
pwd
tar cf - . | (cd  $RPM_BUILD_ROOT%{_prefix}; tar xfp - )
mkdir -p $RPM_BUILD_ROOT%{_sbindir}
mkdir -p $RPM_BUILD_ROOT%{_modules}
mkdir -p $RPM_BUILD_ROOT%{_systemd}
mkdir -p $RPM_BUILD_ROOT%{_udev}
mkdir -p $RPM_BUILD_ROOT%{_bitfilerepo}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/checkAndFixWzdmaPresence.sh $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/deploy_scouting_firmware.sh $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/disableFatalErrorReporting.sh $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/disableFatalErrorReportingForScouting.sh $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/pcie_reconnect_xilinx.sh $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/disconnectPciDeviceFromTree.sh $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/executeScriptOnScoutingDevices.sh $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/pcie_reconnect_hal.sh $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/pcie_reconnect_hal.py $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/prog_clock_Si570.py $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/prog_clock_Si5341.py $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/scripts/I2CInterface.py $RPM_BUILD_ROOT%{_sbindir}
mv $RPM_BUILD_ROOT%{_homedir}/modules/kcu1500_xdma.conf $RPM_BUILD_ROOT%{_modules}
mv $RPM_BUILD_ROOT%{_homedir}/systemd/checkAndFixWzdmaPresence.service $RPM_BUILD_ROOT%{_systemd}
mv $RPM_BUILD_ROOT%{_homedir}/systemd/disableFatalErrorReporting.service $RPM_BUILD_ROOT%{_systemd}
mv $RPM_BUILD_ROOT%{_homedir}/udev/99-xdma-permissions.rules $RPM_BUILD_ROOT%{_udev}

%clean
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT || :

%pre

# Upgrading
if [ $1 -gt 1 ] ; then
  systemctl disable --now checkAndFixWzdmaPresence
  systemctl disable --now disableFatalErrorReporting
  systemctl daemon-reload
fi

%post

# Register the service
systemctl daemon-reload
# This will install and load the driver if it's not present already.
systemctl enable --now checkAndFixWzdmaPresence
systemctl enable --now disableFatalErrorReporting

%preun

# Stopping service before removal
if [ $1 -eq 0 ] ; then
  systemctl stop checkAndFixWzdmaPresence
  systemctl stop disableFatalErrorReporting

  # Unload driver
  modprobe -r xdma
fi

%postun

# Only for uninstall!
if [ $1 -eq 0 ] ; then

  # Removing folder
  systemctl daemon-reload
  rm -fR %{_homedir}
  rmdir --ignore-fail-on-non-empty %{_prefix}

  # Remove driver
  rm -f /lib/modules/$(uname -r)/extra/xdma.ko

fi


/*
 * This file is part of the Xilinx DMA IP Core driver tools for Linux
 *
 * Copyright (c) 2016-present,  Xilinx, Inc.
 * All rights reserved.
 *
 * This source code is licensed under both the BSD-style license (found in the
 * LICENSE file in the root directory of this source tree) and the GPLv2 (found
 * in the COPYING file in the root directory of this source tree).
 * You may select, at your option, one of the above-listed licenses.
 */

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <limits.h>

#include <xdma-ioctl.h>


/*
 * man 2 write:
 * On Linux, write() (and similar system calls) will transfer at most
 * 	0x7ffff000 (2,147,479,552) bytes, returning the number of bytes
 *	actually transferred.  (This is true on both 32-bit and 64-bit
 *	systems.)
 */

#define RW_MAX_SIZE	0x7ffff000

int verbose = 0;

uint64_t getopt_integer(char *optarg)
{
	int rc;
	uint64_t value;

	rc = sscanf(optarg, "0x%lx", &value);
	if (rc <= 0)
		rc = sscanf(optarg, "%lu", &value);
	//printf("sscanf() = %d, value = 0x%lx\n", rc, value);

	return value;
}

ssize_t write_from_buffer(char *fname, int fd, char *buffer, uint64_t size)
{
	ssize_t rc;
	uint64_t count = 0;
	char *buf = buffer;

	while (count < size) {
		int64_t bytes = size - count;

		if (bytes > RW_MAX_SIZE)
			bytes = RW_MAX_SIZE;

		/* write data to file from memory buffer */
		rc = write(fd, buf, bytes);
		if (rc != bytes) {
			fprintf(stderr, "%s, W 0x%lx != 0x%lx.\n",
				fname, rc, bytes);
				perror("write file");
			return -EIO;
		}

		count += bytes;
		buf += bytes;
	}	 

	if (count != size) {
		fprintf(stderr, "%s, R failed 0x%lx != 0x%lx.\n",
				fname, count, size);
		return -EIO;
	}
	return count;
}


/* Subtract timespec t2 from t1
 *
 * Both t1 and t2 must already be normalized
 * i.e. 0 <= nsec < 1000000000
 */
static int timespec_check(struct timespec *t)
{
	if ((t->tv_nsec < 0) || (t->tv_nsec >= 1000000000))
		return -1;
	return 0;

}

void timespec_sub(struct timespec *t1, struct timespec *t2)
{
	if (timespec_check(t1) < 0) {
		fprintf(stderr, "invalid time #1: %lld.%.9ld.\n",
			(long long)t1->tv_sec, t1->tv_nsec);
		return;
	}
	if (timespec_check(t2) < 0) {
		fprintf(stderr, "invalid time #2: %lld.%.9ld.\n",
			(long long)t2->tv_sec, t2->tv_nsec);
		return;
	}
	t1->tv_sec -= t2->tv_sec;
	t1->tv_nsec -= t2->tv_nsec;
	if (t1->tv_nsec >= 1000000000) {
		t1->tv_sec++;
		t1->tv_nsec -= 1000000000;
	} else if (t1->tv_nsec < 0) {
		t1->tv_sec--;
		t1->tv_nsec += 1000000000;
	}
}

/*****************************************************************************/

struct wz_private {
	struct wz_xdma_data_block_desc bdesc __attribute__ ((aligned (8) ));
	struct wz_xdma_data_block_confirm bconf __attribute__ ((aligned (8) ));
	int fd_user;
	int fd_control;
	int fd_memory;
	volatile uint32_t *usr_regs;
	volatile char *data_buf;
	volatile uint64_t dummy1;
};

int wz_init(struct wz_private* wz) 
{
	int res;

	wz->fd_user = -1;
	wz->fd_control = -1;
	wz->fd_memory = -1;
	wz->usr_regs = NULL;
	wz->data_buf = NULL;
	wz->dummy1 = 0;

	if ( (wz->fd_user = open("/dev/wz-xdma0_user", O_RDWR)) < 0 ) {
		perror("Can't open /dev/wz-xdma0_user");
		exit(1);
	};

	if ( (wz->fd_control = open("/dev/wz-xdma0_control", O_RDWR)) < 0 ) {
		perror("Can't open /dev/wz-xdma0_control");
		exit(1);
	};

	if ( (wz->fd_memory = open("/dev/wz-xdma0_c2h_0", O_RDWR)) < 0 ) {
		perror("Can't open /dev/wz-xdma0_c2h_0");
		exit(1);
	};

	//Allocate buffers
	if ( (res = ioctl(wz->fd_memory, IOCTL_XDMA_WZ_ALLOC_BUFFERS, 0L)) < 0 ) {
		perror("I can't alloc DMA buffers");
		exit(3);
	}

	//Now mmap the user registers
	if ( (wz->usr_regs = mmap(NULL, 1024*1024, PROT_READ | PROT_WRITE, MAP_SHARED, wz->fd_user, 0)) == MAP_FAILED) {
		perror("Can't mmap user registers");
		exit(2);		
	}

	printf("%ld\n", TOT_BUF_LEN);
	if ( (wz->data_buf = mmap(NULL, TOT_BUF_LEN, PROT_READ|PROT_WRITE, MAP_SHARED, wz->fd_memory, 0)) == MAP_FAILED) { 
		perror("Can't mmap data");
		exit(2);		
	}

	//Ensure, that all pages are mapped
	{
		uint64_t i;
		for(i=0; i < TOT_BUF_LEN/sizeof(uint64_t); i++)
			wz->dummy1 += wz->data_buf[i];
	}

	return 0;
}

int wz_close(struct wz_private* wz) 
{
	munmap((void *)wz->data_buf, TOT_BUF_LEN);
	munmap((void *)wz->usr_regs, 1024*1024);
	ioctl(wz->fd_memory, IOCTL_XDMA_WZ_FREE_BUFFERS, 0L);
	close(wz->fd_memory);
	close(wz->fd_control);
	close(wz->fd_user);
	return 0;
}

inline int wz_start_dma(struct wz_private* wz)
{
	return ioctl(wz->fd_memory, IOCTL_XDMA_WZ_START, 0L);
}

inline int wz_stop_dma(struct wz_private* wz)
{
	return ioctl(wz->fd_memory, IOCTL_XDMA_WZ_STOP, 0L);
}

static inline int wz_get_buf(struct wz_private* wz) 
{
	return ioctl(wz->fd_memory, IOCTL_XDMA_WZ_GETBUF, (long) &wz->bdesc);
}

static inline int wz_confirm_buf(struct wz_private* wz)
{		
	return ioctl(wz->fd_memory, IOCTL_XDMA_WZ_CONFIRM, (long) &wz->bconf);
}


/* Acquire and return buffer */
inline ssize_t wz_read_start(struct wz_private* wz, char **buffer)
{
	int ret;
	if ( (ret = wz_get_buf( wz )) < 0) {
		return ret;
	}

	int64_t start_offset = (int64_t)wz->bdesc.first_desc * (int64_t)WZ_DMA_BUFLEN;
	int64_t end_offset   = (int64_t)wz->bdesc.last_desc * (int64_t)WZ_DMA_BUFLEN + (int64_t) wz->bdesc.last_len;
	int64_t bytes_read = end_offset - start_offset;

	*buffer = (char *)(wz->data_buf + start_offset);

	return bytes_read;
}


/* Mark the buffer to be available for DMA */
inline int wz_read_complete(struct wz_private* wz)
{
	wz->bconf.first_desc = wz->bdesc.first_desc;
	wz->bconf.last_desc = wz->bdesc.last_desc;

	return wz_confirm_buf( wz );
}




/* 
 * The user logic is driving custom FPGA logic. This is not necessary to use if not implemented in the FPGA.
 */

inline void wz_start_source(struct wz_private* wz)
{
	wz->usr_regs[0x10000/4] = 1;
	asm volatile ("" : : : "memory");
}

// User logic
inline void wz_stop_source(struct wz_private* wz)
{
	wz->usr_regs[0x10000/4] = 0;
	asm volatile ("" : : : "memory");
}

